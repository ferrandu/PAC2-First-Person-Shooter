# PAC2-juego-FPS

Juego First Person Shooter 3D estilo realista en el que el usuario deberá superar una sèrie de retos relacionados en coger objetos de vida, munición, etc. y derrotar enemigos que va a encontrar por el escenario. 
El escenario, la ambientación y la música estan inspirados a juegos como Alien y Resident Evil.

![menu_screen](https://user-images.githubusercontent.com/49591290/57569181-8956f200-73f1-11e9-9296-d72861da2d32.jpg)


# Como jugar
Descargar y extraer el fichero.zip y ejecutar el fichero "AlienusEscape.exe" dentro de la carperta Build. 
Dependiendo del hardware del usuario se recomienda jugar en very low quality para mayor fluidez de movimiento y disparo (no haya delay).


# Controles de juego
Para el movimiento del player se pueden usar las flechas o las teclas WASD.

Para correr mantener tecla "Shift" o "Mayus".

Para apuntar con arma desplazamiento horizontal/vertical del ratón.

Para disparar arma pulsar tecla izquierda del ratón.

Para cambiar de arma pulsar la tecla "Control/Ctrl". Hay que tener en cuenta que hay dos tipos de arma; la pistola y la metralleta.

Pulsar la tecla "Esc" para ir al menú principal.

![controls_720](https://user-images.githubusercontent.com/49591290/57568917-02544a80-73ee-11e9-8689-0f3402fa841c.png)





# Puntos básicos de programación que se han implementado
1. **Player**
*  Se han implementado Script **FPS Controller** para el movimiento del personaje en primera persona y vision y rotación de la camara.
*  Scripts **ShootGun** y **ShootMachineGun** para el disparo con cada una de las armas (pistola y metralleta).
*  Script **ChangeWeapon** para cambiar de arma pulsando tecla control.
*  Script **PlayerHealth** para tener la vida y la armadura del player y recibir daño/vida en ambos.

2. **Enemigos**
*  Para la implementación del robot enemigo se han tenido en cuenta varios Scripts con funcionalidades distintas:
*  Script **EnemyAIDemo** se ha utilizado para cada uno de los enemigos y controla cada uno de los estados inflingiendo daño al Player. Los estados que controla son
*  el **AlertStateDemo**, **IdleStateDemo** y el **EnemyStateDemo**.
*  Script **BossFollow** se utiliza para activar el desplazamiemto del enemigo Alien Boss que al colisonar con el Player se pierde el juego.
*  Script **ColliderBoss** se utiliza para activar el collider que activará la aparición del Alien Boss que empazara a perseguir el Player.

3. **Scripts control de objetos**
*  En esta parte se ha utilizado varios Scripts para la obtención de munición, vida, armadura y objetos clave para solventar los puzzles/retos:
*  Scripts **AmmoGun** y **AmmoMachineGun** se encargan de rellenar la munición total de cada una de las armas respectivamente (pistola y metrallreta).
*  Los Scripts **GetArmor** y **GetHealth** se utilizan para restaurar totalmente las barras de vida y armadura del Player al detectar colision. 
*  Script **AutomaticDoors** se encarga de mostrar de abrir las puertas automaticamente cuando detecta la colision del Player.
*  El Script **CardDoor** se encarga de recoger la targeta para abrir una de las puertas y mostrarla en el canvas.
*  El Script **DoorOpenCard** se encarga de comprobar si el Player ha obtenido la targeta para posteriormente abrir la puerta mediante animación.
*  El Script **Battery** se utiliza para obtener el objeto de la bateria y mostrarla en el canvas.
*  El Script **FinalGame** se encarga de detecar si el Player colisiona teniendo el objeto de la bateria recogida para posteriormente poder escapar con la nave mediante animación. 

4. **Controles de pantallas**
*  Por lo que refiere a la navegación de las pantallas del Menu y Controles del juego se ha utilizado el Script **Menu**.
*  Para la gestión de la pantalla de menú una vez ha empezado el juego se ha usado el Script **MenuInGame**.
 

# Puntos extra que se han implementado
1. **Boss Alien**: Para la parte final del juego se ha optado crear un enemigo final como Alien que ayuda mucho a generar esa tensión que se buscaba desde un inicio. Además
es una especie de Boss invencible al que sólo puedes escapar de él. Su figura está muy inspirada con el boss "Némesis" del juego de ResidentEvil 3 al que te aperece en
determinados momentos del juego presiguiendo al Player.
2. **Efectos de Postprocesado**: Se ha optado por utilizar varios efectos de postprocesado como el ambient oclusion, depth of field, color grading, vignette, etc, para generar
más tensión y suspense en la escena. 

![game_screen](https://user-images.githubusercontent.com/49591290/57569245-5c570f00-73f2-11e9-804a-18052fe0350f.jpg)

3. **Musicas y Efectos de Sonido**: Para generar la ambientación realista de tensión y suspense que se buscava desde un incio se han utilizado soundtracks  y efectos de sonido
 parecidos a los juegos como Alien o Resident Evil tanto en las pantallas de menú y controles como en la pantalla de juego.
4. **Partículas y Skybox**: Como aspectos visuales se ha optado de canviar la default Skybox de Unity por otra Skybox con cielo nublado más adiente para el juego al igual que
se ha generado varios sistemas de partículas para crear explosiones cuando se destruyen los robots enemigos, el avión accidentado, la bateria y las chispas de la nave de Alien.
Las partículas ayudan a generar más realismo y immersión en el juego.

# Link al video de gameplay del juego y comentarios de lo que se ha implementado
[Video Gameplay Alienus Escape (FPS)](https://www.youtube.com/watch?v=in8HN1I9llY&feature=youtu.be)
